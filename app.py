
''' libraries needed:
pip install flask
pip install db-sqlite3
'''

from flask import Flask
from flask import render_template
from flask import redirect
from flask import request
from flask import session
from flask import url_for
from flask import flash
import os
import logging
from lib.PDBC import PDBC


app = Flask(__name__)

# allows to set or access the session dictionary
app.secret_key = "secretkey"

# disable cache css/js to reload modified css/js on all pages
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

# allow the information view about applications runtime ~ print in console
logging.basicConfig(level=logging.DEBUG)


# create sqlite database object
pdbc = PDBC("db/sqlite_database.db")



@app.route('/')
def index():
    return render_template("index.html")

@app.errorhandler(404)
def page_not_found(error):
    return 'ERROR 404'


@app.route('/home', methods=['GET', 'POST'])
def home():
	if request.method == 'POST':
		name = request.form['name']
		app.logger.info(name)
		if name == "" :
			# return redirect("/hello_withoutname")
			return redirect(url_for("hello_name", name="no name"))
		else :
			return redirect("/hello_{}".format(name))
	return render_template("home.html")



@app.route('/hello')
def hello():
	msg = "Hello World!"
	app.logger.info("Processing default request --> {}".format(msg))
	flash("Test flash message success", "success")
	flash("Test 2 success", "success")
	flash("Test flash message error", "error")
	flash("Test 2 error", "error")
	return render_template("hello.html", msg=msg)

@app.route('/hello_<name>')
def hello_name(name):
	msg = "Hello {}!".format(name)
	app.logger.info("Processing default request --> {}".format(msg))
	return render_template("hello.html", msg=msg)



@app.route('/tests/database')
def test_database():
	from tests.test_database import test_database_connection
	pdbc.open()
	result = test_database_connection(pdbc)
	pdbc.close()
	return render_template("tests/test_database.html" , result=result)

@app.route('/jeux/taquin')
def jeux_taquin():
	return render_template("jeux/taquin.html")

@app.route('/jeux/jeu-de-la-vie')
def jeux_jeu_de_la_vie():
	listPatterns = os.listdir("static/pattern/gameoflife")
	return render_template('jeux/jeu-de-la-vie.html', listPatterns=listPatterns)

""" CURRENT """

@app.route('/front/home')
def hompage():
	return render_template("current/front/home.html")

@app.route('/front/connexion')
def connexion():
	return render_template("current/front/connexion.html")

@app.route('/front/spots')
def spots():
	return render_template("current/front/spots.html")


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=80, debug=True, threaded=True)
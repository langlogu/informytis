
import sqlite3

class PDBC :
	'''
	Connection class to a sqlite database

	(insert update delete create drop) sql methods via internal method
	'''

	def __init__(self, database) :
		self.__database = database
		self.__connection = None
		self.__isConnected = False

	def open(self) :
		if not self.__isConnected :
			self.__connection = sqlite3.connect(self.__database)
			self.__isConnected = True
		return True

	def close(self) :
		if self.__isConnected :
			self.__connection.close()
			self.__isConnected = False
		return True

	def getConnection(self) :
		return self.__connection if self.__isConnected else None

	def getCursor(self) :
		if self.__isConnected :
			return self.__connection.cursor()
		else :
			return None

	def commit(self) :
		if self.__isConnected :
			self.__connection.commit()
			return True
		else :
			return False

	def isConnected(self) :
		return self.__isConnected

	def request_insert_update_delete_create_drop(self, request) :
		execute = False
		try:
		    cursor = self.__connection.cursor()
		    cursor.execute(request)
		    cursor.close()
		    self.__connection.commit()
		    execute = True
		except sqlite3.Error as error:
		    print("Failed to execute request : {}".format(error))
		return execute



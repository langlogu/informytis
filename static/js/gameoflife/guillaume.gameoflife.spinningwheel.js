

var runSpinningWheel = false;
var divSpinningWheel;
var stageSpinningWheel;
var layerSpinningWheel;
var gridBlackRectSpinningWheel;
var lastGameArraySpinningWheel;
var gridSpinningWheel = {
	x: 13,
	y: 13,
}
var patternSpinningWheel = [
	[0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,1,1,0,1,1,1,1,1,1,0,0],
	[0,0,1,1,0,1,1,1,1,1,1,0,0],
	[0,0,1,1,0,0,0,0,0,0,0,0,0],
	[0,0,1,1,0,0,0,0,0,1,1,0,0],
	[0,0,1,1,0,0,0,0,0,1,1,0,0],
	[0,0,1,1,0,0,0,0,0,1,1,0,0],
	[0,0,0,0,0,0,0,0,0,1,1,0,0],
	[0,0,1,1,1,1,1,1,0,1,1,0,0],
	[0,0,1,1,1,1,1,1,0,1,1,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0],
];

function loadSliderSpinningWheel(divId) { //onload in body to load js after html page
	divSpinningWheel = divId;
	initStageSpinningWheel();
	runSpinningWheel = true;
	gameLoopSpinningWheel();
}

function initStageSpinningWheel() {
	stageSpinningWheel = new Konva.Stage({
		container: divSpinningWheel,
		width: 50,
		height: 50,
		draggable: false,
	});
	stageSpinningWheel.attrs.x = 0;
	stageSpinningWheel.attrs.y = 0;
	layerSpinningWheel = new Konva.Layer();

	lastGameArraySpinningWheel = [];
	gridBlackRectSpinningWheel = [];
	for (var x = 0; x < gridSpinningWheel.x; x++) {
		lastGameArraySpinningWheel[x] = [];
		gridBlackRectSpinningWheel[x] = [];
		for (var y = 0; y < gridSpinningWheel.y; y++) {
			gridBlackRectSpinningWheel[x][y] = new Konva.Rect({
					x: x * stageSpinningWheel.getWidth()/gridSpinningWheel.x,
					y: y * stageSpinningWheel.getHeight()/gridSpinningWheel.y,
					width: stageSpinningWheel.getWidth()/gridSpinningWheel.x,
					height: stageSpinningWheel.getHeight()/gridSpinningWheel.y,
					fill: 'lightblue',
					stroke: 'black',
					strokeWidth: 0.1,
					strokeEnabled: true,
				});
			lastGameArraySpinningWheel[x][y] = 0;
			gridBlackRectSpinningWheel[x][y].visible(false);
			if (y < patternSpinningWheel.length && x < patternSpinningWheel[0].length && patternSpinningWheel[y][x] == 1) {
				lastGameArraySpinningWheel[x][y] = 1;
				gridBlackRectSpinningWheel[x][y].visible(true);
			}
			layerSpinningWheel.add(gridBlackRectSpinningWheel[x][y]);
		}
	}

	/* pointer click event */
	stageSpinningWheel.on('click', function (e) {
		var clickPos = {
			x: (stageSpinningWheel.getPointerPosition().x - stageSpinningWheel.attrs.x)/stageSpinningWheel.scaleX() - gridBlackRectSpinningWheel[0][0].x(),
			y: (stageSpinningWheel.getPointerPosition().y - stageSpinningWheel.attrs.y)/stageSpinningWheel.scaleY() - gridBlackRectSpinningWheel[0][0].y(),
		}; //console.log('click stageSpinningWheel on ' + JSON.stringify(clickPos));
		if (clickPos.x >= 0 && clickPos.y >= 0 && clickPos.x <= stageSpinningWheel.getWidth() && clickPos.y <= stageSpinningWheel.getHeight()) {
			var clickBox = {
				x: Math.trunc(clickPos.x * gridSpinningWheel.x / stageSpinningWheel.getWidth()),
				y: Math.trunc(clickPos.y * gridSpinningWheel.y / stageSpinningWheel.getHeight()),
			}; //console.log('click on ' + JSON.stringify(clickBox));
			if (!gridBlackRectSpinningWheel[clickBox.x][clickBox.y].isVisible()) {
				lastGameArraySpinningWheel[clickBox.x][clickBox.y] = 1;
				gridBlackRectSpinningWheel[clickBox.x][clickBox.y].visible(true);
			} else {
				lastGameArraySpinningWheel[clickBox.x][clickBox.y] = 0;
				gridBlackRectSpinningWheel[clickBox.x][clickBox.y].visible(false);
			}
			stageSpinningWheel.batchDraw(); //console.log("CLICK"); console.log(lastGameArraySpinningWheel);
		} 
	});

	stageSpinningWheel.add(layerSpinningWheel);
	// stageSpinningWheel.batchDraw();
}

function gameLoopSpinningWheel() {
	if (!runSpinningWheel) {
		return;
	}
	// console.log("pass");
    nextRoundSpinningWheel();
    setTimeout(gameLoopSpinningWheel, 100);
}

function runDrawSpinningWheel() {
	runSpinningWheel = !runSpinningWheel;
	if (runSpinningWheel) {
		gameLoopSpinningWheel();
	}
}

function nextRoundSpinningWheel() {
	for (var x = 0; x < gridSpinningWheel.x; x++) {
		for (var y = 0; y < gridSpinningWheel.y; y++) {
			var nbVoisinesActives = 0;
			for (var i = -1; i <= 1; i++) {
				for (var j = -1; j <= 1; j++) {
					if ((i==0 && j==0) || (x+i<0) || (x+i>=gridSpinningWheel.x) || (y+j<0) || (y+j>=gridSpinningWheel.y)) {
						continue;
					}
					var xx = (x + i + gridSpinningWheel.x) % gridSpinningWheel.x;
					var yy = (y + j + gridSpinningWheel.y) % gridSpinningWheel.y;
					nbVoisinesActives = nbVoisinesActives + lastGameArraySpinningWheel[xx][yy];
				}
			}
			if (lastGameArraySpinningWheel[x][y] == 1) {
				if (nbVoisinesActives > 3 || nbVoisinesActives < 2) {
					gridBlackRectSpinningWheel[x][y].visible(false);
				}
			} else {
				if (nbVoisinesActives == 3) {
					gridBlackRectSpinningWheel[x][y].visible(true);
				}
			}
		}
	}
	for (var x = 0; x < gridSpinningWheel.x; x++) {
		for (var y = 0; y < gridSpinningWheel.y; y++) {
			lastGameArraySpinningWheel[x][y] = (gridBlackRectSpinningWheel[x][y].isVisible()) ? 1 : 0;
		}
	}
	stageSpinningWheel.batchDraw();
}




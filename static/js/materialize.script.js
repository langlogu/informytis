// Custom Materialize JavaScript
(function($){
    $(function(){

      $('.sidenav').sidenav();
      
      $(".dropdown-trigger").dropdown({ closeOnClick: true, constrainWidth: false, coverTrigger: false });

      $('.tabs').tabs();
      

    }); // end of document ready
})(jQuery); // end of jQuery name space


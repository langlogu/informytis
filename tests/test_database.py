


def test_database_connection(pdbc):
	result = "----------\n\n"

	result = result + "{} {}\n".format("Open",pdbc.open())
	result = result + "{} {} {}\n".format("Connected",pdbc.isConnected(),pdbc.getConnection())
	result = result + "{} {}\n".format("Commit",pdbc.commit())
	result = result + "{} {}\n\n".format("Close",pdbc.close())
	result = result + "{} {} {}\n".format("Connected",pdbc.isConnected(),pdbc.getConnection())
	result = result + "{} {}\n".format("Commit",pdbc.commit())
	result = result + "{} {}\n\n----------\n\n".format("Close",pdbc.close())

	result = result + "{} {}\n".format("Open",pdbc.open())
	result = result + "{} {} {}\n".format("Connected",pdbc.isConnected(),pdbc.getConnection())
	result = result + "{} {}\n".format("Commit",pdbc.commit())
	result = result + "{} {}\n\n".format("Close",pdbc.close())
	result = result + "{} {} {}\n".format("Connected",pdbc.isConnected(),pdbc.getConnection())
	result = result + "{} {}\n".format("Commit",pdbc.commit())
	result = result + "{} {}\n\n----------\n\n".format("Close",pdbc.close())

	result = result + "{} {}\n\n".format("Open",pdbc.open())
	result = result + "{} {}\n\n".format(
		"CREATE TEST_TABLE",
		pdbc.request_insert_update_delete_create_drop(
			"CREATE TABLE TEST_TABLE (id INTEGER PRIMARY KEY)"
		)
	)
	result = result + "{} {}\n\n".format(
		"CREATE TEST_TABLE",
		pdbc.request_insert_update_delete_create_drop(
			"CREATE TABLE TEST_TABLE (id INTEGER PRIMARY KEY)"
		)
	)
	result = result + "{} {}\n\n".format(
		"DROP TEST_TABLE",
		pdbc.request_insert_update_delete_create_drop(
			"DROP TABLE TEST_TABLE"
		)
	)
	result = result + "{} {}\n\n----------\n".format(
		"DROP TEST_TABLE",
		pdbc.request_insert_update_delete_create_drop(
			"DROP TABLE TEST_TABLE"
		)
	)

	return result



# INDEPENDENT TEST
'''
import sys
sys.path.insert(0,'../lib/')

import sqlite3
from PDBC import PDBC

pdbc = PDBC("../db/sqlite_database.db")
result = test_database_connection(pdbc)
print(result)
'''
